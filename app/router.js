import React, { useRef } from "react";

import useCurrentUrl from "shared/hooks/use-current-url";
import useInitialProps from "shared/hooks/use-initial-props";
import { Provider as HistoryProvider } from "shared/hooks/use-history";
import useRoute from "shared/hooks/use-route";

export default function Router({ history, ...props }) {
  let isFirstRenderRef = useRef(true);
  let url = useCurrentUrl(history);
  let Component = DefaultRouter;
  if (isFirstRenderRef.current) {
    isFirstRenderRef.current = false;
    Component = FirstRenderRouter;
  }
  return (
    <HistoryProvider value={history}>
      <Component history={history} url={url} {...props} />
    </HistoryProvider>
  );
}

function FirstRenderRouter({ url, initialProps, ...props }) {
  let { Component, getInitialProps } = useRoute(url);

  if (!initialProps && getInitialProps) {
    return <DefaultRouter url={url} />;
  }
  if (!Component) {
    return null;
  }
  return <Component {...initialProps} />;
}

function DefaultRouter({ url, ...props }) {
  let { Component, initialProps, isLoading, isLoaded } = useInitialProps(url);

  if (isLoading) {
    return "Loading ...";
  }
  if (!isLoaded) {
    return "Error...";
  }
  if (!Component) {
    return null;
  }
  return <Component {...initialProps} />;
}
