import UrlPattern from "url-pattern";

import RepositoryOverviewPage from "shared/views/pages/repository-overview";
import RepositoryDetailPage from "shared/views/pages/repository-detail";

const route = (pattern, component) => ({
  pattern: new UrlPattern(pattern),
  component
});

export default [
  route("/", RepositoryOverviewPage),
  route("/repos/:owner/:repo", RepositoryDetailPage)
];
