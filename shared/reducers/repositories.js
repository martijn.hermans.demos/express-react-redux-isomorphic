import { createReducer } from '@touchtribe/redux-helpers'
import * as Actions from 'shared/actions'

const initialState = []

function onFetchAllSuccess (state, action) {
  return action.repositories
}

function onFetchAllFail (state, action) {
  return action.error
}

export default createReducer('repositories', {
  [Actions.fetchRepositoriesSuccess]: onFetchAllSuccess,
  [Actions.fetchRepositoriesFail]: onFetchAllFail,
}, initialState)
