import { createReducer } from '@touchtribe/redux-helpers'
import * as Actions from 'shared/actions'

const initialState = {}

function onFetchSuccess (state, {owner, name, repository}) {
  return {
    ...state,
    [`${owner}/${name}`]: repository
  }
}

function onFetchFail (state, {owner, name, error}) {
  return {
    ...state,
    [`${owner}/${name}`]: error
  }
}

export default createReducer('repository', {
  [Actions.fetchRepositorySuccess]: onFetchSuccess,
  [Actions.fetchRepositoryFail]: onFetchFail,
}, initialState)
