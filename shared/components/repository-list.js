import React from "react";
import Link from "shared/components/link";

export default function RepositoryList({ repositories, ...props }) {
  return (
    <div className="row">
      {repositories.map(repository => (
        <div className="col-12" key={repository.id}>
          <RepositoryListCard repository={repository} />
        </div>
      ))}
    </div>
  );
}
function RepositoryListCard({ repository }) {
  return (
    <div className="media">
      <img
        src={repository.owner.avatar_url}
        alt=""
        className="mr-3"
        style={{ maxWidth: "40px", maxHeight: "40px" }}
      />
      <div className="media-body">
        <Link href={`/repos/${repository.full_name}`}>
          <h5 className="mt-0">{repository.full_name}</h5>
        </Link>
        <p>{repository.description.substr(0, 40)}</p>
      </div>
    </div>
  );
}
