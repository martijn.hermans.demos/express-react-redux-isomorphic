import { useState, useEffect } from "react";
import useRoute from "shared/hooks/use-route";
import { useDispatch } from 'react-redux'

export default function useInitialProps(url) {
  let { Component: matchedComponent, getInitialProps, matches } = useRoute(url);

  let [{ initialProps, isLoading, isLoaded, Component }, setState] = useState(
    {}
  );
  let dispatch = useDispatch()

  useEffect(() => {
    if (!getInitialProps) {
      setState({
        initialProps: {},
        Component: matchedComponent,
        isLoaded: true
      });
    } else {
      setState({ isLoading: true });
      getInitialProps({ req: { params: matches }, dispatch }).then(initialProps =>
        setState({
          initialProps,
          Component: matchedComponent,
          isLoaded: true
        })
      );
    }
  }, [matchedComponent, getInitialProps, matches]);
  return { Component, initialProps, isLoading, isLoaded };
}
