import { useMemo } from "react";
import Routes from "shared/config/routes";
import { getRoute } from "shared/utils/get-route";

export default function useRoute(url) {
  const { matches, Component, getInitialProps } = useMemo(
    () => getRoute(url, Routes),
    [url]
  );
  return {
    matches,
    Component,
    getInitialProps
  };
}
