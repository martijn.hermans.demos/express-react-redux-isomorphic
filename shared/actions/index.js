import { createAction } from '@touchtribe/redux-helpers'
import * as Api from 'shared/utils/api'

export const fetchRepositories = (query) => async (dispatch) => {
  try {
    let repositories = await Api.fetchRepositories(query)

    await dispatch(fetchRepositoriesSuccess(repositories))
  } catch (error) {
    await dispatch(fetchRepositoriesFail(error))

  }
}

export const fetchRepositoriesSuccess = createAction(
  'repositories//fetch/success',
  (repositories) => ({ repositories })
)
export const fetchRepositoriesFail = createAction(
  'repositories//fetch/fail',
  (error) => ({ error })
)

export const fetchRepository = (owner, name) => async (dispatch) => {
  try {
    let repository = await Api.fetchRepository(owner, name)

    await dispatch(fetchRepositorySuccess(owner, name, repository))
  } catch (error) {
    await dispatch(fetchRepositoryFail(owner, name, error))

  }
}

export const fetchRepositorySuccess = createAction(
  'repository//fetch/success',
  (owner, name, repository) => ({ owner, name, repository })
)
export const fetchRepositoryFail = createAction(
  'repository//fetch/fail',
  (owner, name, error) => ({ owner, name, error })
)
