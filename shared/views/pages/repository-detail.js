import React from 'react'
import * as Actions from 'shared/actions'
import Link from 'shared/components/link'
import { useSelector } from 'react-redux'

export default function RepositoryDetailPage ({ owner, repo }) {
  const repository = useSelector(state => state.repository[`${owner}/${repo}`])
  return (
    <div className="container">
      <h3>{repository.name}</h3>
      <p>{repository.description}</p>
      <Link href="/">Back to overview</Link>
    </div>
  )
}

RepositoryDetailPage.getInitialProps = async ({ req, dispatch }) => {
  const { owner, repo } = req.params

  await dispatch(Actions.fetchRepository(owner, repo))

  return { owner, repo }
}
