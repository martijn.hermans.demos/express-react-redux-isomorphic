import React from 'react'
import { useSelector } from 'react-redux'
import * as Actions from 'shared/actions'
import RepositoryList from 'shared/components/repository-list'

export default function RepositoryOverviewPage ({ query }) {
  let repositories = useSelector(state => state.repositories)

  repositories = Array.isArray(repositories) ? repositories : []

  return (
    <div className="container">
      <h3>{query}</h3>
      <RepositoryList repositories={repositories} />
    </div>
  )
}

RepositoryOverviewPage.getInitialProps = async ({ dispatch }) => {
  const query = 'react'

  await dispatch(Actions.fetchRepositories(query))

  return { query }
}
