export function getRoute(url, routes) {
  let { pattern, component: Component } =
    routes.find(({ pattern }) => pattern.match(url)) || {};
  if (!pattern) {
    return {};
  }
  let matches = pattern.match(url);
  return {
    pattern,
    matches,
    Component,
    getInitialProps: Component.getInitialProps
  };
}
