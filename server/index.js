require("@babel/register")({
  presets: [
    "@babel/preset-react",
    [
      "@babel/env",
      {
        targets: {
          node: true
        }
      }
    ]
  ],
  plugins: [
    [
      "module-resolver",
      {
        alias: {
          "server": "./server",
          "app": "./src",
          "shared": "./shared"
        }
      }
    ]
  ]
});
require("./server");
