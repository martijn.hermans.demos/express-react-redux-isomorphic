export default function createDocument({
  title,
  html,
  jsSrc,
  cssSrc,
  initialProps,
  preloadedState
}) {
  return `
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    ${title ? `<title>${title}</title>` : ""}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    ${cssSrc ? `<link rel="stylesheet" href="${cssSrc}" >` : ""}
  </head>
  <body>
  
    <div id="react-app">
      ${html}
    </div>
    <script>window.__INITIAL_PROPS__ = ${JSON.stringify(initialProps)}</script>
    <script>window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState)}</script>
    ${jsSrc ? `<script src="${jsSrc}"></script>` : ""}
  </body>
</html>
  `;
}
