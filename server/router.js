import { Router } from 'express'
import React from 'react'
import ReactDOM from 'react-dom/server'
import { Provider as ReduxProvider } from 'react-redux'

import createDocument from './create-document'

import Routes from 'shared/config/routes'
import { getRoute } from 'shared/utils/get-route'
import createStore from 'shared/store'

const router = Router()
export default router

router.get('*', async (req, res) => {
  const { matches, Component, getInitialProps } =
  getRoute(req.originalUrl, Routes) || {}

  const store = createStore()

  let initialProps = {}

  if (getInitialProps) {
    initialProps = await getInitialProps({
      req: { params: matches },
      dispatch: (...args) => store.dispatch(...args)
    })
  }

  const html = ReactDOM.renderToString(
    <ReduxProvider store={store}>
      <Component {...initialProps} />
    </ReduxProvider>
  )

  res.send(
    createDocument({
      html,
      jsSrc: '/assets/js/main.js',
      initialProps,
      preloadedState: store.getState()
    })
  )
})
